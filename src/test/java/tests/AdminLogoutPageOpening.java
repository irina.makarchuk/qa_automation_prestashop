package tests;

import org.junit.Test;
import preconditions.AddCategoryPage;
import preconditions.AdminDashboardPage;
import preconditions.CategoriesPage;
import preconditions.Login;

import static org.junit.Assert.assertTrue;

public class AdminLogoutPageOpening extends Login {
    @Test
    public void checkIfTheCategoryIsAdded() {
        AdminDashboardPage dashboardPage = new AdminDashboardPage(driver);
        AddCategoryPage addCategoryPage = new AddCategoryPage(driver);
        CategoriesPage categoriesPage = dashboardPage.proceedToCategoriesSubmenu();
        categoriesPage.clickAddCategory();
        addCategoryPage.addNewCategory();
        addCategoryPage.getCategories();
        assertTrue("The category hasn't been added. ", addCategoryPage.isCategoryAdded());
    }
}